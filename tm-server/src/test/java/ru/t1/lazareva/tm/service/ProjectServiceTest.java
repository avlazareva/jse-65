package ru.t1.lazareva.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.lazareva.tm.api.service.dto.IProjectDtoService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.lazareva.tm.exception.field.DescriptionEmptyException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.NameEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;

import java.util.Collection;

import static ru.t1.lazareva.tm.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest extends AbstractSchemeTest {

    @NotNull
    @Autowired
    private IProjectDtoService service;

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void before() throws Exception {
        userService.add(USER_1);
        userService.add(USER_2);
        service.add(USER_1.getId(), USER_PROJECT1);
        service.add(USER_2.getId(), USER_PROJECT2);
    }

    @After
    public void after() throws Exception {
        service.clear();
        userService.clear();
    }

    @Test
    public void add() throws Exception {
        service.clear();
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_PROJECT1.getName());
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.create(USER_PROJECT1.getUserId(), null);
        });
        service.clear();
        Assert.assertNotNull(service.create(USER_PROJECT1.getUserId(), USER_PROJECT1.getName()));
        @Nullable final ProjectDto project = service.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.create(USER_1.getId(), NULL_PROJECT.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_PROJECT1.getName());
        });
        service.clear();
        Assert.assertNotNull(service.add(USER_1.getId(), USER_PROJECT1));
        @Nullable final ProjectDto project = service.findOneById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findAll() {
        @NotNull final Collection<ProjectDto> projectsFindAllNoEmpty = service.findAll();
        Assert.assertNotNull(projectsFindAllNoEmpty);
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USER_1.getId(), "");
        });
        Assert.assertFalse(service.existsById(USER_1.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(USER_1.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.findOneById(NON_EXISTING_PROJECT_ID);
        });
        @Nullable final ProjectDto project = service.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_1.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_PROJECT1.getId());
        });
        Assert.assertNull(service.findOneById(USER_1.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDto project = service.findOneById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void clear() throws Exception {
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
        service.clear();
        service.add(USER_1.getId(), USER_PROJECT1);
        service.clear(USER_1.getId());
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.remove(null);
        });
        @NotNull final ProjectDto createdProject = service.create(USER_1.getId(), USER_PROJECT1.getName());
        service.remove(createdProject);
        Assert.assertNotEquals(0, service.findAll(USER_PROJECT1.getUserId()).size());
    }

    @Test
    public void removeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", null);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.remove(USER_1.getId(), null);
        });
        service.clear();
        @NotNull final ProjectDto createdProject = service.create(USER_1.getId(), USER_PROJECT1.getName());
        service.remove(USER_1.getId(), createdProject);
        Assert.assertNull(service.findOneById(USER_1.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_1.getId(), "");
        });
        service.removeById(USER_1.getId(), USER_PROJECT1.getId());
        Assert.assertNull(service.findOneById(USER_1.getId(), USER_PROJECT1.getId()));
        @Nullable final ProjectDto createdProject = service.add(USER_PROJECT1);
        service.removeById(USER_1.getId(), createdProject.getId());
        Assert.assertNull(service.findOneById(USER_1.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void getSize() throws Exception {
        service.clear();
        service.add(USER_PROJECT1);
        Assert.assertEquals(1, service.getSize());
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_PROJECT1.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", USER_PROJECT1.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(USER_1.getId(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(USER_1.getId(), "");
        });
        @NotNull final ProjectDto project = service.create(USER_1.getId(), USER_PROJECT1.getName());
        Assert.assertEquals(project.getId(), service.findOneById(USER_1.getId(), project.getId()).getId());
        Assert.assertEquals(USER_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER_1.getId(), project.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(USER_1.getId(), null, USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(USER_1.getId(), "", USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(USER_1.getId(), USER_PROJECT1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create(USER_1.getId(), USER_PROJECT1.getName(), "");
        });
        @NotNull final ProjectDto project = service.create(USER_1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        Assert.assertEquals(project.getId(), service.findOneById(USER_1.getId(), project.getId()).getId());
        Assert.assertEquals(USER_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER_1.getId(), project.getUserId());
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USER_1.getId(), null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USER_1.getId(), "", USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USER_1.getId(), USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USER_1.getId(), USER_PROJECT1.getId(), "", USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.updateById(USER_1.getId(), NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        @NotNull final String name = USER_PROJECT1.getName();
        @NotNull final String description = USER_PROJECT1.getDescription();
        service.updateById(USER_1.getId(), USER_PROJECT1.getId(), name, description);
        Assert.assertEquals(name, USER_PROJECT1.getName());
        Assert.assertEquals(description, USER_PROJECT1.getDescription());
    }

    @Test
    public void changeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.NOT_STARTED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById(null, USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById("", USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(USER_1.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(USER_1.getId(), "", status);
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.changeProjectStatusById(USER_1.getId(), NON_EXISTING_PROJECT_ID, status);
        });
        service.changeProjectStatusById(USER_1.getId(), USER_PROJECT1.getId(), status);
        Assert.assertNotNull(USER_PROJECT1);
        Assert.assertEquals(status, USER_PROJECT1.getStatus());
    }

}