package ru.t1.lazareva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.UserDto;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static String USER_TEST_LOGIN = "test";

    @NotNull
    public final static String USER_TEST_PASSWORD = "test";

    @NotNull
    public final static String USER_TEST_EMAIL = "test@test.ru";

    @NotNull
    public final static String ADMIN_TEST_LOGIN = "admin";

    @NotNull
    public final static String ADMIN_TEST_PASSWORD = "admin";

    @NotNull
    public final static String ADMIN_TEST_EMAIL = "admin@admin.ru";

    @NotNull
    public final static UserDto USER_1 = new UserDto(USER_TEST_LOGIN, USER_TEST_PASSWORD, USER_TEST_EMAIL);

    @NotNull
    public final static UserDto USER_2 = new UserDto(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
    @NotNull
    public final static List<UserDto> USER_LIST = Arrays.asList(USER_1, USER_2);
    @Nullable
    public final static UserDto NULL_USER = null;
    @NotNull
    public final static String NON_EXISTING_USER_ID = UUID.randomUUID().toString();
    @NotNull
    public final static String NON_EXISTING_USER_LOGIN = "123";

}
