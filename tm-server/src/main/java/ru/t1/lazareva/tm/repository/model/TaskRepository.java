package ru.t1.lazareva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.model.Task;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    @NotNull
    Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    );

    @NotNull
    Task create(@NotNull final String userId, @NotNull final String name);

    @NotNull
    Class<Task> getClazz();

    @Nullable
    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @NotNull
    String getSortColumnName(@NotNull final Comparator comparator);

    void removeAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

}
