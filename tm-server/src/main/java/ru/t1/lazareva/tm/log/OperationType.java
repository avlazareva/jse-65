package ru.t1.lazareva.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}