package ru.t1.lazareva.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @SneakyThrows
    @Transactional
    M add(M model) throws Exception;

    @SneakyThrows
    @Transactional
    void clear() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable List<M> findAll() throws Exception;

    @Nullable List<M> findAll(@Nullable Comparator comparator) throws Exception;

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

    M findOneById(@Nullable String id) throws Exception;

    long getSize() throws Exception;

    @SneakyThrows
    @Transactional
    void remove(M model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @SneakyThrows
    @Transactional
    M update(M model) throws Exception;
}


