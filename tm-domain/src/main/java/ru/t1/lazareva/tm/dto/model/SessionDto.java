package ru.t1.lazareva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity
@Table(name = "tm.tm_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class SessionDto extends AbstractUserOwnedModelDto {

    @NotNull
    @Column(nullable = false, name = "session_date")
    private Date date = new Date();

    @Nullable
    @Column(nullable = true, name = "role")
    @Enumerated(EnumType.STRING)
    private Role role = null;

    public SessionDto(@NotNull final UserDto user) {
        this.setUserId(user.getId());
    }
}
