package ru.t1.lazareva.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.lazareva.tm.component.Bootstrap;
import ru.t1.lazareva.tm.configuration.LoggerConfiguration;

public final class Application {

    public static void main(@Nullable String... args) throws InterruptedException {
        Thread.sleep(System.getenv().containsKey("LATENCY") ? Integer.valueOf(System.getenv("LATENCY")) : 10000);
        @NotNull ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
