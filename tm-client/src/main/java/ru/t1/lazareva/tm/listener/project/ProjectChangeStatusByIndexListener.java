package ru.t1.lazareva.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.event.ConsoleEvent;
import ru.t1.lazareva.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByIndexListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-change-status-by-index";

    @NotNull
    private static final String DESCRIPTION = "Change project status by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @EventListener(condition = "@projectChangeStatusByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(getToken());
        request.setIndex(index);
        request.setStatus(status);
        projectEndpoint.changeProjectStatusByIndex(request);
    }

}