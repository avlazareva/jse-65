package ru.t1.lazareva.tm.api.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.dto.request.UserLoginRequest;
import ru.t1.lazareva.tm.dto.request.UserLogoutRequest;
import ru.t1.lazareva.tm.dto.request.UserViewProfileRequest;
import ru.t1.lazareva.tm.dto.response.UserLoginResponse;
import ru.t1.lazareva.tm.dto.response.UserLogoutResponse;
import ru.t1.lazareva.tm.dto.response.UserViewProfileResponse;

public interface IAuthEndpointClient extends IEndpointClient {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserViewProfileResponse viewProfile(@NotNull UserViewProfileRequest request);

}

